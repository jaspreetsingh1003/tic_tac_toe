//
//  StartupScreenView.m
//  TicTacToe
//
//  Created by click labs136 on 10/13/15.
//  Copyright (c) 2015 click labs136. All rights reserved.
//

#import "StartupScreenView.h"


@implementation StartupScreenView
{
    PlayerDetailsView *playerDetailsViewObject;
    SettingsView *settingsViewObject;

}

-(void)startupScreenElements{

    UIImageView* backgroungImage= [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)];
    [backgroungImage setImage:[UIImage imageNamed:@"901-Cartoon-Clouds-l.jpg"]];
//    [backgroungImage setUserInteractionEnabled:true];
    [self addSubview:backgroungImage];
    
    
    UILabel* ticTacToeLabel= [[UILabel alloc]initWithFrame:CGRectMake(40, 25, 300, 200)];
    [ticTacToeLabel setText:@"Tic Tac Toe"];
    [ticTacToeLabel setBackgroundColor:[UIColor clearColor]];
    [ticTacToeLabel setFont:[UIFont fontWithName:@"helvetica" size:50]];
    [ticTacToeLabel setTextColor:[UIColor whiteColor]];
    [self addSubview:ticTacToeLabel];
    
    UILabel* lastWinner= [[UILabel alloc]initWithFrame:CGRectMake(40, 500, 200, 40)];
    [lastWinner setText:@"Last Winner: "];
    [lastWinner setTextColor:[UIColor whiteColor]];
    [lastWinner setBackgroundColor:[UIColor clearColor]];
    [self addSubview:lastWinner];
    
    UIButton* startButton= [[UIButton alloc]initWithFrame:CGRectMake(130, 200, 50, 50)];
    [startButton setBackgroundColor:[UIColor clearColor]];
    [startButton setImage:[UIImage imageNamed:@"02_play-512.png"] forState:UIControlStateNormal];
    [startButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [startButton addTarget:self action:@selector(startButtonAction) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:startButton];
    
    
    UIButton* settingsButton= [[UIButton alloc]initWithFrame:CGRectMake(120, 280, 80, 80)];
    [settingsButton setBackgroundColor:[UIColor clearColor]];
    [settingsButton setImage:[UIImage imageNamed:@"Setting-icon.png"] forState:UIControlStateNormal];
    [settingsButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [settingsButton addTarget:self action:@selector(settingButtonAction) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:settingsButton];
    
   
    
//    playerDetailsViewObject=[[PlayerDetailsView alloc]initWithFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)];
//    [playerDetailsViewObject playerDetailsViewElements];
//    [self addSubview:playerDetailsViewObject];
   
    
}

-(void)startButtonAction{

    [self.delegate startButtonAction];
    
}

-(void)settingButtonAction{

    [self.delegate settingButtonAction];
    
//    settingsViewObject= [[SettingsView alloc]initWithFrame:CGRectMake(00, 00, self.frame.size.width, self.frame.size.height)];
//    settingsViewObject.backgroundColor=[UIColor grayColor];
//    [settingsViewObject settingsViewElements];
//    [self addSubview:settingsViewObject];
    
}


@end
