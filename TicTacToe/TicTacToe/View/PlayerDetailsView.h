//
//  PlayerDetailsView.h
//  TicTacToe
//
//  Created by click labs136 on 10/13/15.
//  Copyright (c) 2015 click labs136. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol PlayerDetailsViewDelegateProtocol <NSObject>

-(void)PlayerDetailsbackButtonAction;
-(void)PlayerDetailsstartGameButtonAction;

@end

@interface PlayerDetailsView : UIView<UITextFieldDelegate>

-(void)playerDetailsViewElements;

@property (nonatomic,assign)id<PlayerDetailsViewDelegateProtocol>delegatePlayerDetail;

@end
