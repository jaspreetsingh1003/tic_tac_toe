//
//  SettingsView.h
//  TicTacToe
//
//  Created by click labs136 on 10/13/15.
//  Copyright (c) 2015 click labs136. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>


@protocol SettingsViewDelegateProtocol <NSObject,AVAudioPlayerDelegate>

-(void)backButtonAction;

@end

@interface SettingsView : UIView{

AVAudioPlayer *gameSound;

}

-(void)settingsViewElements;

@property(nonatomic,assign)id<SettingsViewDelegateProtocol>delgateback;


@end
