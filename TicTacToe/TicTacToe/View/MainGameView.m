//
//  MainGameView.m
//  TicTacToe
//
//  Created by click labs136 on 10/13/15.
//  Copyright (c) 2015 click labs136. All rights reserved.
//

#import "MainGameView.h"


@implementation MainGameView{

    UIButton *button100;
    UIButton *button101;
    UIButton *button102;
    UIButton *button110;
    UIButton *button111;
    UIButton *button112;
    UIButton *button120;
    UIButton *button121;
    UIButton *button122;
    int Player;
    int scorecheck;
    UIButton *zero;
    UIButton *cross;
    NSInteger players;
    NSInteger round;
    UIButton *imagePlaced;
    UILabel *playernameLabel;
}

-(void)mainGameViewElements{
    
    Player=1;
    scorecheck=0;
    
    UIImageView* backgroungImage= [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)];
    [backgroungImage setImage:[UIImage imageNamed:@"901-Cartoon-Clouds-l.jpg"]];
    //    [backgroungImage setUserInteractionEnabled:true];
    [self addSubview:backgroungImage];
    
//    UIImageView* backgroundImage =[[UIImageView alloc]initWithFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)];
//    [backgroundImage setImage:[UIImage imageNamed:@"tic-tac-toe-symbol-yellow-pencils-6328844.jpeg"]];
//    [self addSubview:backgroundImage];
    
    UIButton *backButton=[[UIButton alloc]initWithFrame:CGRectMake(20, 20, 30, 30)];
    [backButton setImage:[UIImage imageNamed:@"usability_bidirectionality_guidelines_when1.png"] forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(mainGameBackButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:backButton];
    
    
    playernameLabel = [[UILabel alloc]initWithFrame:CGRectMake(self.frame.origin.x+20, self.frame.origin.y+20, self.frame.size.width-60, 40)];
    [playernameLabel setText:@"Turn"];
    [playernameLabel setTextAlignment:NSTextAlignmentCenter];
    [playernameLabel setTextColor:[UIColor whiteColor]];
    [self addSubview:playernameLabel];
    [self switchPlayer];
    
 
    
    UIBezierPath *pathHorizontal1 = [UIBezierPath bezierPath];
    [pathHorizontal1 moveToPoint:CGPointMake(self.frame.origin.x+20, 250.0)];
    [pathHorizontal1 addLineToPoint:CGPointMake(300.0, 170.0)];
    CAShapeLayer *shapeLayerHorizontal1 = [CAShapeLayer layer];
    shapeLayerHorizontal1.path = [pathHorizontal1 CGPath];
    shapeLayerHorizontal1.strokeColor = [[UIColor whiteColor] CGColor];
//    colorWithRed:22/255.0f green:51/255.0f blue:86/255.0f alpha:1.0f
    shapeLayerHorizontal1.lineWidth = 1.0;
    shapeLayerHorizontal1.fillColor = [[UIColor clearColor] CGColor];
    [self.layer addSublayer:shapeLayerHorizontal1];
    
    UIBezierPath *pathHorizontal2 = [UIBezierPath bezierPath];
    [pathHorizontal2 moveToPoint:CGPointMake(self.frame.origin.x+20, 350.0)];
    [pathHorizontal2 addLineToPoint:CGPointMake(300.0, 330.0)];
    CAShapeLayer *shapeLayerHorizontal2 = [CAShapeLayer layer];
    shapeLayerHorizontal2.path = [pathHorizontal2 CGPath];
    shapeLayerHorizontal2.strokeColor = [[UIColor whiteColor] CGColor];
    //    colorWithRed:22/255.0f green:51/255.0f blue:86/255.0f alpha:1.0f
    shapeLayerHorizontal2.lineWidth = 1.0;
    shapeLayerHorizontal2.fillColor = [[UIColor clearColor] CGColor];
    [self.layer addSublayer:shapeLayerHorizontal2];

    UIBezierPath *pathVertical1 = [UIBezierPath bezierPath];
    [pathVertical1 moveToPoint:CGPointMake(self.frame.origin.x+100, 100.0)];
    [pathVertical1 addLineToPoint:CGPointMake(self.frame.origin.x+80, 450)];
    CAShapeLayer *shapeLayerVertical1 = [CAShapeLayer layer];
    shapeLayerVertical1.path = [pathVertical1 CGPath];
    shapeLayerVertical1.strokeColor = [[UIColor whiteColor] CGColor];
    //    colorWithRed:22/255.0f green:51/255.0f blue:86/255.0f alpha:1.0f
    shapeLayerVertical1.lineWidth = 1.5;
    shapeLayerVertical1.fillColor = [[UIColor clearColor] CGColor];
    [self.layer addSublayer:shapeLayerVertical1];

    UIBezierPath *pathVertical2 = [UIBezierPath bezierPath];
    [pathVertical2 moveToPoint:CGPointMake(self.frame.origin.x+240, 100.0)];
    [pathVertical2 addLineToPoint:CGPointMake(self.frame.origin.x+220, 450)];
    CAShapeLayer *shapeLayervertical2 = [CAShapeLayer layer];
    shapeLayervertical2.path = [pathVertical2 CGPath];
    shapeLayervertical2.strokeColor = [[UIColor whiteColor] CGColor];
    //    colorWithRed:22/255.0f green:51/255.0f blue:86/255.0f alpha:1.0f
    shapeLayervertical2.lineWidth = 1.5;
    shapeLayervertical2.fillColor = [[UIColor clearColor] CGColor];
    [self.layer addSublayer:shapeLayervertical2];
    
    
    
    button100 =[[UIButton alloc]initWithFrame:CGRectMake(20, 120, 60, 100)];
//    [button1by1 setImage:[UIImage imageNamed:@"IMG_20151014_122616.png"] forState:UIControlStateNormal];
    [button100 addTarget:self action:@selector(button1by1Action) forControlEvents:UIControlEventTouchUpInside];
    button100.tag=100;
    [self addSubview:button100];
    
    button101 =[[UIButton alloc]initWithFrame:CGRectMake(90, 120, 150, 100)];
//    [button2by1 setImage:[UIImage imageNamed:@"IMG_20151014_122616.png"] forState:UIControlStateNormal];
    [button101 addTarget:self action:@selector(button2by1Action) forControlEvents:UIControlEventTouchUpInside];
    button101.tag=101;
    [self addSubview:button101];
    
    
    button102 =[[UIButton alloc]initWithFrame:CGRectMake(240, 100, 70, 80)];
//    [button3by1 setImage:[UIImage imageNamed:@"IMG_20151014_122616.png"] forState:UIControlStateNormal];
    [button102 addTarget:self action:@selector(button3by1Action) forControlEvents:UIControlEventTouchUpInside];
    button102.tag=102;
    [self addSubview:button102];
    
    button110 =[[UIButton alloc]initWithFrame:CGRectMake(20, 250, 60, 120)];
//    [button1by2 setImage:[UIImage imageNamed:@"IMG_20151014_122616.png"] forState:UIControlStateNormal];
    [button110 addTarget:self action:@selector(button1by2Action) forControlEvents:UIControlEventTouchUpInside];
    button110.tag=110;
    [self addSubview:button110];
    
    button111 =[[UIButton alloc]initWithFrame:CGRectMake(90, 200, 150, 160)];
//    [button2by2 setImage:[UIImage imageNamed:@"IMG_20151014_122616.png"] forState:UIControlStateNormal];
    [button111 addTarget:self action:@selector(button2by2Action) forControlEvents:UIControlEventTouchUpInside];
    button111.tag=111;
    [self addSubview:button111];
    
    button112 =[[UIButton alloc]initWithFrame:CGRectMake(240, 200, 70, 140)];
//    [button3by2 setImage:[UIImage imageNamed:@"IMG_20151014_122616.png"] forState:UIControlStateNormal];
    [button112 addTarget:self action:@selector(button3by2Action) forControlEvents:UIControlEventTouchUpInside];
    button112.tag=112;
    [self addSubview:button112];
    
    button120 =[[UIButton alloc]initWithFrame:CGRectMake(20, 350, 70, 100)];
//    [button1by3 setImage:[UIImage imageNamed:@"IMG_20151014_122616.png"] forState:UIControlStateNormal];
    [button120 addTarget:self action:@selector(button1by3Action) forControlEvents:UIControlEventTouchUpInside];
    button120.tag=120;
    [self addSubview:button120];
    
    button121 =[[UIButton alloc]initWithFrame:CGRectMake(90, 350, 110, 110)];
//    [button2by3 setImage:[UIImage imageNamed:@"IMG_20151014_122616.png"] forState:UIControlStateNormal];
    [button121 addTarget:self action:@selector(button2by3Action) forControlEvents:UIControlEventTouchUpInside];
    button121.tag=121;
    [self addSubview:button121];
    
    button122 =[[UIButton alloc]initWithFrame:CGRectMake(240, 350, 70, 140)];
//    [button3by3 setImage:[UIImage imageNamed:@"IMG_20151014_122616.png"] forState:UIControlStateNormal];
    [button122 addTarget:self action:@selector(button3by3Action) forControlEvents:UIControlEventTouchUpInside];
    button122.tag=123;
    [self addSubview:button122];
    

    
}

-(void)switchPlayer{

    if (Player==1) {
        Player=2;
        playernameLabel.text=@"Player 1";
    } else {Player=1;
    playernameLabel.text = @"Player 2 ";
    }
}


-(void)setImage{

    [zero setImage:[UIImage imageNamed:@"0.png"] forState:UIControlStateNormal];
    
    [cross setImage:[UIImage imageNamed:@"X.png"] forState:UIControlStateNormal];
    
    players = 1;
    
    round = 0 ;
    
    playernameLabel.text= @"Player 1";

}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{


    if (players==1) {
        imagePlaced = zero;
    }
    else if (players==2){
    
        imagePlaced=cross;
    }
    
}

#pragma marks - All button Actions

-(void)mainGameBackButtonAction:(id)sender{
    
    [self.mainDelegate mainGameBack];
//    [self removeFromSuperview];
    
    
}


-(void)button1by1Action{
    
    if ( ! button100.imageView.image) {
        if ( Player==1) {
            UIImage *imageObject2= [UIImage imageNamed:@"0.png"];
            [button100 setImage: imageObject2 forState:UIControlStateNormal];
            [self switchPlayer];
            [self winningStatus];
        }
        else if (Player==2){
            [button100 setImage:[UIImage imageNamed:@"X.png"] forState:UIControlStateNormal];
            [self switchPlayer];
            [self winningStatus];
        }
    }
}

-(void)button2by1Action{
    
    if (! button101.imageView.image) {
        if ( Player==1) {
            UIImage *imageObject2= [UIImage imageNamed:@"0.png"];
            [button101 setImage: imageObject2 forState:UIControlStateNormal];
            [self switchPlayer];
            [self winningStatus];
        }
        else if (Player==2){
            [button101 setImage:[UIImage imageNamed:@"X.png"] forState:UIControlStateNormal];
            [self switchPlayer];
            [self winningStatus];
        }
    }
}


-(void)button3by1Action{
    
    if ( ! button102.imageView.image ) {
        if ( Player==1) {
            UIImage *imageObject= [UIImage imageNamed:@"0.png"];
            [button102 setImage: imageObject forState:UIControlStateNormal];
            [self switchPlayer];
            [self winningStatus];
        }
        else if (Player==2){
            [button102 setImage:[UIImage imageNamed:@"X.png"] forState:UIControlStateNormal];
            [self switchPlayer];
            [self winningStatus];
        }
    }
}


-(void)button1by2Action{
    
    if (! button110.imageView.image) {
        if ( Player==1) {
            UIImage *imageObject= [UIImage imageNamed:@"0.png"];
            [button110 setImage: imageObject forState:UIControlStateNormal];
            [self switchPlayer];
            [self winningStatus];
        }
        else if (Player==2){
            [button110 setImage:[UIImage imageNamed:@"X.png"] forState:UIControlStateNormal];
            [self switchPlayer];
            [self winningStatus];
        }
    }
}


-(void)button2by2Action{
    if ( !button111.imageView.image) {
        if ( Player==1) {
            UIImage *imageObject= [UIImage imageNamed:@"0.png"];
            [button111 setImage: imageObject forState:UIControlStateNormal];
            [self switchPlayer];
            [self winningStatus];
        }
        else if (Player==2){
            [button111 setImage:[UIImage imageNamed:@"X.png"] forState:UIControlStateNormal];
            [self switchPlayer];
            [self winningStatus];
        }
    }
}



-(void)button3by2Action{
    if (! button112.imageView.image) {
        if ( Player==1) {
            UIImage *imageObject= [UIImage imageNamed:@"0.png"];
            [button112 setImage: imageObject forState:UIControlStateNormal];
            [self switchPlayer];
            [self winningStatus];
        }
        else if (Player==2){
            [button112 setImage:[UIImage imageNamed:@"X.png"] forState:UIControlStateNormal];
            [self switchPlayer];
            [self winningStatus];
        }
    }
}



-(void)button1by3Action{
    
    if ( !button120.imageView.image) {
        if ( Player==1) {
            UIImage *imageObject= [UIImage imageNamed:@"0.png"];
            [button120 setImage: imageObject forState:UIControlStateNormal];
            [self switchPlayer];
            [self winningStatus];
        }
        else if (Player==2){
            [button120 setImage:[UIImage imageNamed:@"X.png"] forState:UIControlStateNormal];
            [self switchPlayer];
            [self winningStatus];
        }
    }
}



-(void)button2by3Action{
    
    if ( ! button121.imageView.image) {
        if ( Player==1) {
            UIImage *imageObject= [UIImage imageNamed:@"0.png"];
            [button121 setImage: imageObject forState:UIControlStateNormal];
            [self switchPlayer];
            [self winningStatus];
        }
        else if (Player==2){
            [button121 setImage:[UIImage imageNamed:@"X.png"] forState:UIControlStateNormal];
            [self switchPlayer];
            [self winningStatus];
        }
    }
}


-(void)button3by3Action{
    
    if (! button122.imageView.image) {
        
        if ( Player==1) {
            UIImage *imageObject= [UIImage imageNamed:@"0.png"];
            [button122 setImage: imageObject forState:UIControlStateNormal];
            [self switchPlayer];
            [self winningStatus];
        }
        else if (Player==2){
            [button122 setImage:[UIImage imageNamed:@"X.png"] forState:UIControlStateNormal];
            [self switchPlayer];
            [self winningStatus];
        }
    }
}


-(void)winningStatus{
 
    scorecheck++;
    NSLog(@"Scorecheck is %d", scorecheck);
    if (scorecheck==9) {
        
        if ([button100.imageView.image isEqual:[UIImage imageNamed:@"X.png"]] && [button101.imageView.image isEqual:[UIImage imageNamed:@"X.png"]] && [button102.imageView.image isEqual:[UIImage imageNamed:@"X.png"]]){
            // NSLog(@"x1 match");
            
            UIImageView *tempView = [[UIImageView alloc]initWithFrame:CGRectMake(10, 195,300, 3)];
            tempView.backgroundColor = [UIColor redColor];
            [self addSubview:tempView];
            
            UIAlertView *winner = [[UIAlertView alloc]initWithTitle:@"Winner" message:@"You win" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
            [winner show];
            
        }else if([button110.imageView.image isEqual:[UIImage imageNamed:@"X.png"]] && [button111.imageView.image isEqual:[UIImage imageNamed:@"X.png"]] && [button112.imageView.image isEqual:[UIImage imageNamed:@"X.png"]]){
            //NSLog(@"x2 match");
            
            UIImageView *tempView = [[UIImageView alloc]initWithFrame:CGRectMake(10, 295,300, 3)];
            tempView.backgroundColor = [UIColor redColor];
            [self addSubview:tempView];
            
            UIAlertView *winner = [[UIAlertView alloc]initWithTitle:@"Winner" message:@"You win" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
            [winner show];
            
        }else if([button120.imageView.image isEqual:[UIImage imageNamed:@"X.png"]] && [button121.imageView.image isEqual:[UIImage imageNamed:@"X.png"]] && [button122.imageView.image isEqual:[UIImage imageNamed:@"X.png"]]){
            //NSLog(@"x3 match");
            
            UIImageView *tempView = [[UIImageView alloc]initWithFrame:CGRectMake(10, 395,300, 3)];
            tempView.backgroundColor = [UIColor redColor];
            [self addSubview:tempView];
            
            UIAlertView *winner = [[UIAlertView alloc]initWithTitle:@"Winner" message:@"You win" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
            [winner show];
        }
        else if ([button100.imageView.image isEqual:[UIImage imageNamed:@"X.png"]] && [button110.imageView.image isEqual:[UIImage imageNamed:@"X.png"]] && [button120.imageView.image isEqual:[UIImage imageNamed:@"X.png"]]){
            //NSLog(@"x4 match");
            
            UIImageView *tempView = [[UIImageView alloc]initWithFrame:CGRectMake(65, 115,3, 340)];
            tempView.backgroundColor = [UIColor redColor];
            [self addSubview:tempView];
            
            UIAlertView *winner = [[UIAlertView alloc]initWithTitle:@"Winner" message:@"You win" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
            [winner show];
        }
        
        else if ([button101.imageView.image isEqual:[UIImage imageNamed:@"X.png"]] && [button111.imageView.image isEqual:[UIImage imageNamed:@"X.png"]] && [button121.imageView.image isEqual:[UIImage imageNamed:@"X.png"]]){
            //NSLog(@"x5 match");
            
            UIImageView *tempView = [[UIImageView alloc]initWithFrame:CGRectMake(160, 115,3, 340)];
            tempView.backgroundColor = [UIColor redColor];
            [self addSubview:tempView];
            
            UIAlertView *winner = [[UIAlertView alloc]initWithTitle:@"Winner" message:@"You win" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
            [winner show];
        }
        
        else if([button102.imageView.image isEqual:[UIImage imageNamed:@"X.png"]] && [button112.imageView.image isEqual:[UIImage imageNamed:@"X.png"]] && [button122.imageView.image isEqual:[UIImage imageNamed:@"X.png"]]){
            //NSLog(@"x6 match");
            
            UIImageView *tempView = [[UIImageView alloc]initWithFrame:CGRectMake(255, 115,3, 340)];
            tempView.backgroundColor = [UIColor redColor];
            [self addSubview:tempView];
            
            UIAlertView *winner = [[UIAlertView alloc]initWithTitle:@"Winner" message:@"You win" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
            [winner show];
            
        }
        
        else  if ([button100.imageView.image isEqual:[UIImage imageNamed:@"X.png"]] && [button111.imageView.image isEqual:[UIImage imageNamed:@"X.png"]] && [button122.imageView.image isEqual:[UIImage imageNamed:@"X.png"]]){
            //NSLog(@"x7 match");
            
            UIBezierPath *pathHorizontal1 = [UIBezierPath bezierPath];
            [pathHorizontal1 moveToPoint:CGPointMake(25, 155.0)];
            [pathHorizontal1 addLineToPoint:CGPointMake(295.0, 435.0)];
            CAShapeLayer *shapeLayerHorizontal1 = [CAShapeLayer layer];
            shapeLayerHorizontal1.path = [pathHorizontal1 CGPath];
            shapeLayerHorizontal1.strokeColor = [[UIColor redColor] CGColor];
            shapeLayerHorizontal1.lineWidth = 3.0;
            shapeLayerHorizontal1.fillColor = [[UIColor clearColor] CGColor];
            [self.layer addSublayer:shapeLayerHorizontal1];
            
            UIAlertView *winner = [[UIAlertView alloc]initWithTitle:@"Winner" message:@"You win" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
            [winner show];
        }
        
        else if([button102.imageView.image isEqual:[UIImage imageNamed:@"X.png"]] && [button111.imageView.image isEqual:[UIImage imageNamed:@"X.png"]] && [button120.imageView.image isEqual:[UIImage imageNamed:@"X.png"]])
        {
            
            UIBezierPath *pathHorizontal1 = [UIBezierPath bezierPath];
            [pathHorizontal1 moveToPoint:CGPointMake(290, 150.0)];
            [pathHorizontal1 addLineToPoint:CGPointMake(20.0, 435.0)];
            CAShapeLayer *shapeLayerHorizontal1 = [CAShapeLayer layer];
            shapeLayerHorizontal1.path = [pathHorizontal1 CGPath];
            shapeLayerHorizontal1.strokeColor = [[UIColor redColor] CGColor];
            shapeLayerHorizontal1.lineWidth = 3.0;
            shapeLayerHorizontal1.fillColor = [[UIColor clearColor] CGColor];
            [self.layer addSublayer:shapeLayerHorizontal1];
            
            UIAlertView *winner = [[UIAlertView alloc]initWithTitle:@"Winner" message:@"You win" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
            [winner show];
            
            // NSLog(@"x8 match");
        }
        
        
        ///////////////    ///////////////    ///////////////    ///////////////    ///////////////
        
        else if ([button100.imageView.image isEqual:[UIImage imageNamed:@"0.png"]] && [button101.imageView.image isEqual:[UIImage imageNamed:@"0.png"]] && [button102.imageView.image isEqual:[UIImage imageNamed:@"0.png"]]){
            // NSLog(@"x1 match");
            
            UIImageView *tempView = [[UIImageView alloc]initWithFrame:CGRectMake(10, 195,300, 3)];
            tempView.backgroundColor = [UIColor redColor];
            [self addSubview:tempView];
            
            UIAlertView *winner = [[UIAlertView alloc]initWithTitle:@"Winner" message:@"You win" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
            [winner show];
            
        }else if([button110.imageView.image isEqual:[UIImage imageNamed:@"0.png"]] && [button111.imageView.image isEqual:[UIImage imageNamed:@"0.png"]] && [button112.imageView.image isEqual:[UIImage imageNamed:@"0.png"]]){
            //NSLog(@"x2 match");
            
            UIImageView *tempView = [[UIImageView alloc]initWithFrame:CGRectMake(10, 295,300, 3)];
            tempView.backgroundColor = [UIColor redColor];
            [self addSubview:tempView];
            
            UIAlertView *winner = [[UIAlertView alloc]initWithTitle:@"Winner" message:@"You win" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
            [winner show];
            
        }else if([button120.imageView.image isEqual:[UIImage imageNamed:@"0.png"]] && [button121.imageView.image isEqual:[UIImage imageNamed:@"0.png"]] && [button122.imageView.image isEqual:[UIImage imageNamed:@"0.png"]]){
            //NSLog(@"x3 match");
            
            UIImageView *tempView = [[UIImageView alloc]initWithFrame:CGRectMake(10, 395,300, 3)];
            tempView.backgroundColor = [UIColor redColor];
            [self addSubview:tempView];
            
            UIAlertView *winner = [[UIAlertView alloc]initWithTitle:@"Winner" message:@"You win" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
            [winner show];
        }
        else if ([button100.imageView.image isEqual:[UIImage imageNamed:@"0.png"]] && [button110.imageView.image isEqual:[UIImage imageNamed:@"0.png"]] && [button120.imageView.image isEqual:[UIImage imageNamed:@"0.png"]]){
            //NSLog(@"x4 match");
            
            UIImageView *tempView = [[UIImageView alloc]initWithFrame:CGRectMake(65, 115,3, 340)];
            tempView.backgroundColor = [UIColor redColor];
            [self addSubview:tempView];
            
            UIAlertView *winner = [[UIAlertView alloc]initWithTitle:@"Winner" message:@"You win" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
            [winner show];
        }
        
        else if ([button101.imageView.image isEqual:[UIImage imageNamed:@"0.png"]] && [button111.imageView.image isEqual:[UIImage imageNamed:@"0.png"]] && [button121.imageView.image isEqual:[UIImage imageNamed:@"0.png"]]){
            //NSLog(@"x5 match");
            
            UIImageView *tempView = [[UIImageView alloc]initWithFrame:CGRectMake(160, 115,3, 340)];
            tempView.backgroundColor = [UIColor redColor];
            [self addSubview:tempView];
            
            UIAlertView *winner = [[UIAlertView alloc]initWithTitle:@"Winner" message:@"You win" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
            [winner show];
        }
        
        else if([button102.imageView.image isEqual:[UIImage imageNamed:@"0.png"]] && [button112.imageView.image isEqual:[UIImage imageNamed:@"0.png"]] && [button122.imageView.image isEqual:[UIImage imageNamed:@"0.png"]]){
            //NSLog(@"x6 match");
            
            UIImageView *tempView = [[UIImageView alloc]initWithFrame:CGRectMake(255, 115,3, 340)];
            tempView.backgroundColor = [UIColor redColor];
            [self addSubview:tempView];
            
            UIAlertView *winner = [[UIAlertView alloc]initWithTitle:@"Winner" message:@"You win" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
            [winner show];
            
        }
        
        else  if ([button100.imageView.image isEqual:[UIImage imageNamed:@"0.png"]] && [button111.imageView.image isEqual:[UIImage imageNamed:@"0.png"]] && [button122.imageView.image isEqual:[UIImage imageNamed:@"0.png"]]){
            //NSLog(@"x7 match");
            
            UIBezierPath *pathHorizontal1 = [UIBezierPath bezierPath];
            [pathHorizontal1 moveToPoint:CGPointMake(25, 155.0)];
            [pathHorizontal1 addLineToPoint:CGPointMake(295.0, 435.0)];
            CAShapeLayer *shapeLayerHorizontal1 = [CAShapeLayer layer];
            shapeLayerHorizontal1.path = [pathHorizontal1 CGPath];
            shapeLayerHorizontal1.strokeColor = [[UIColor redColor] CGColor];
            shapeLayerHorizontal1.lineWidth = 3.0;
            shapeLayerHorizontal1.fillColor = [[UIColor clearColor] CGColor];
            [self.layer addSublayer:shapeLayerHorizontal1];
            
            UIAlertView *winner = [[UIAlertView alloc]initWithTitle:@"Winner" message:@"You win" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
            [winner show];
        }
        
        else if([button102.imageView.image isEqual:[UIImage imageNamed:@"0.png"]] && [button111.imageView.image isEqual:[UIImage imageNamed:@"0.png"]] && [button120.imageView.image isEqual:[UIImage imageNamed:@"0.png"]])
        {
            
            UIBezierPath *pathHorizontal1 = [UIBezierPath bezierPath];
            [pathHorizontal1 moveToPoint:CGPointMake(290, 150.0)];
            [pathHorizontal1 addLineToPoint:CGPointMake(20.0, 435.0)];
            CAShapeLayer *shapeLayerHorizontal1 = [CAShapeLayer layer];
            shapeLayerHorizontal1.path = [pathHorizontal1 CGPath];
            shapeLayerHorizontal1.strokeColor = [[UIColor redColor] CGColor];
            shapeLayerHorizontal1.lineWidth = 3.0;
            shapeLayerHorizontal1.fillColor = [[UIColor clearColor] CGColor];
            [self.layer addSublayer:shapeLayerHorizontal1];
            
            UIAlertView *winner = [[UIAlertView alloc]initWithTitle:@"Winner" message:@"You win" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
            [winner show];
            
            // NSLog(@"x8 match");
        }
        
    else{
        UIAlertView *winner = [[UIAlertView alloc]initWithTitle:@"Draw" message:@"Draw" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [winner show];
        }
    }
}
    





@end
