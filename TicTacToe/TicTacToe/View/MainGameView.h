//
//  MainGameView.h
//  TicTacToe
//
//  Created by click labs136 on 10/13/15.
//  Copyright (c) 2015 click labs136. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol MainGameViewDelegateProtocol <NSObject>

-(void)mainGameBack;

@end

@interface MainGameView : UIView<UIAlertViewDelegate>
-(void)mainGameViewElements;


@property (nonatomic,assign)id <MainGameViewDelegateProtocol> mainDelegate;


@end
