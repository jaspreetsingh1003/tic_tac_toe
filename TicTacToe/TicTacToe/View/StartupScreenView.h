//
//  StartupScreenView.h
//  TicTacToe
//
//  Created by click labs136 on 10/13/15.
//  Copyright (c) 2015 click labs136. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PlayerDetailsView.h"
#import "SettingsView.h"

@protocol StartupScreenViewDelgatePr <NSObject>

-(void)startButtonAction;
-(void)settingButtonAction;


@end


@interface StartupScreenView : UIView

-(void)startupScreenElements;

@property (nonatomic,assign)id<StartupScreenViewDelgatePr> delegate;



@end
