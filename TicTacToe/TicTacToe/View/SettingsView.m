//
//  SettingsView.m
//  TicTacToe
//
//  Created by click labs136 on 10/13/15.
//  Copyright (c) 2015 click labs136. All rights reserved.
//

#import "SettingsView.h"

@implementation SettingsView


-(void)soundAction{
    
    NSURL *url=[NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"ghostrider_aQX2hMEG" ofType:@"mp3" ]];
    gameSound = [[AVAudioPlayer alloc]initWithContentsOfURL:url error:nil];
    [[AVAudioSession sharedInstance]setCategory:AVAudioSessionCategoryPlayback error:nil];
    [[AVAudioSession sharedInstance]setActive:YES error:nil];
    [[UIApplication sharedApplication]beginReceivingRemoteControlEvents];
    [gameSound setNumberOfLoops:-1];
    gameSound.volume=1;
    [gameSound play];
    [self soundAction];
    
}

-(void)settingsViewElements{
    
    UIImageView* backgroungImage= [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)];
    [backgroungImage setImage:[UIImage imageNamed:@"901-Cartoon-Clouds-l.jpg"]];
    //    [backgroungImage setUserInteractionEnabled:true];
    [self addSubview:backgroungImage];
    
    UILabel* soundLabel= [[UILabel alloc]initWithFrame:CGRectMake(30, 200, 100, 30)];
    [soundLabel setText:@"Sound: "];
    [soundLabel setTextAlignment:NSTextAlignmentCenter];
    [soundLabel setTextColor:[UIColor whiteColor]];
    [soundLabel setBackgroundColor:[UIColor clearColor]];
    [soundLabel setFont:[UIFont fontWithName:@"helvetica" size:25]];
    [self addSubview:soundLabel];
    
//    UISegmentedControl* themeSelectionSegement=[[UISegmentedControl alloc]initWithFrame:CGRectMake(200, 70, 100, 30)];
//    themeSelectionSegement.backgroundColor=[UIColor redColor];
//    [self addSubview:themeSelectionSegement];
    
    
    

    UISwitch *soundSelectionbutton =[[UISwitch alloc]initWithFrame:CGRectMake(220, 200, 50, 30)];
    if ([soundSelectionbutton isOn]) {
        NSLog(@"Switch ON");
    } else {
        
        NSLog(@"Switchoff");
    }
    [self addSubview:soundSelectionbutton];
    
    
    
    UIButton *backButton=[[UIButton alloc]initWithFrame:CGRectMake(20, 20, 30, 30)];
    [backButton setImage:[UIImage imageNamed:@"usability_bidirectionality_guidelines_when1.png"] forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(backButtonAction) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:backButton];
    
    
}

-(void)backButtonAction{

    [self.delgateback backButtonAction];
}

@end
