//
//  PlayerDetailsView.m
//  TicTacToe
//
//  Created by click labs136 on 10/13/15.
//  Copyright (c) 2015 click labs136. All rights reserved.
//

#import "PlayerDetailsView.h"
#import "MainGameView.h"

@implementation PlayerDetailsView
{
    MainGameView *mainGameViewObject;
    UITextField* firstPlayerTextfield;
    UITextField* secondPlayerTextfield;
}

-(void)playerDetailsViewElements{
    
    
    UIImageView* backgroungImage= [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)];
    [backgroungImage setImage:[UIImage imageNamed:@"901-Cartoon-Clouds-l.jpg"]];
    //    [backgroungImage setUserInteractionEnabled:true];
    [self addSubview:backgroungImage];
    
    UILabel* firstPlayerDetailsLabel= [[UILabel alloc]initWithFrame:CGRectMake(30, 100, 100, 40)];
    [firstPlayerDetailsLabel setText:@"First Player:"];
    [firstPlayerDetailsLabel setTextColor:[UIColor whiteColor]];
    [firstPlayerDetailsLabel setBackgroundColor:[UIColor clearColor]];
    [self addSubview:firstPlayerDetailsLabel];
    
    UIImageView* firstplayerDetailsImage= [[UIImageView alloc]initWithFrame:CGRectMake(10, 105, 20, 30)];
    [firstplayerDetailsImage setImage:[UIImage imageNamed:@"O.png"]];
    [self addSubview:firstplayerDetailsImage];
    
  
    
    
    UILabel* secondPlayerDetailsLabel= [[UILabel alloc]initWithFrame:CGRectMake(30, 180, 130, 40)];
    [secondPlayerDetailsLabel setText:@"Second Player:"];
    [secondPlayerDetailsLabel setTextColor:[UIColor whiteColor]];
    [secondPlayerDetailsLabel setBackgroundColor:[UIColor clearColor]];
    [self addSubview:secondPlayerDetailsLabel];
    
    UIImageView* secondPlayerDetailsImage= [[UIImageView alloc]initWithFrame:CGRectMake(10, 185, 20, 30)];
    [secondPlayerDetailsImage setImage:[UIImage imageNamed:@"X.png"]];
    [self addSubview:secondPlayerDetailsImage];
    
    firstPlayerTextfield=[[UITextField alloc]initWithFrame:CGRectMake(150, 100, 150, 40)];
    [firstPlayerTextfield setPlaceholder:@"Enter here"];
    [firstPlayerTextfield setBorderStyle:UITextBorderStyleRoundedRect];
    [firstPlayerTextfield setBackgroundColor:[UIColor whiteColor]];
    [firstPlayerTextfield setTextAlignment:NSTextAlignmentLeft];
    firstPlayerTextfield.delegate= self;
    [self addSubview:firstPlayerTextfield];
    
//    if (firstPlayerTextfield.text && firstPlayerTextfield.text.length > 0)
//    {
//        UIAlertView* alertView1= [[UIAlertView alloc]initWithTitle:@"Name Empty" message:@"Player1 Name cannot be empty" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
//        [alertView1 show];
//    }
    
    
    secondPlayerTextfield=[[UITextField alloc]initWithFrame:CGRectMake(150, 180, 150, 40)];
    [secondPlayerTextfield setPlaceholder:@"Enter here"];
    [secondPlayerTextfield setBorderStyle:UITextBorderStyleRoundedRect];
    [secondPlayerTextfield setBackgroundColor:[UIColor whiteColor]];
    [secondPlayerTextfield setTextAlignment:NSTextAlignmentLeft];
    secondPlayerTextfield.delegate= self;
    [self addSubview:secondPlayerTextfield];
    
    
    
    
    UIButton* startButton= [[UIButton alloc]initWithFrame:CGRectMake(90, 250, 150, 50)];
    [startButton setBackgroundColor:[UIColor clearColor]];
    [startButton setTitle:@"Start Game" forState:UIControlStateNormal];
    [startButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
   [startButton addTarget:self action:@selector(PlayerDetailsstartGameButtonAction) forControlEvents:UIControlEventTouchUpInside];
    //    startButton.contentHorizontalAlignment= UIControlContentHorizontalAlignmentCenter;
    [self addSubview:startButton];
    
    UIButton *backButton=[[UIButton alloc]initWithFrame:CGRectMake(20, 20, 30, 30)];
    [backButton setImage:[UIImage imageNamed:@"usability_bidirectionality_guidelines_when1.png"] forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(PlayerDetailsbackButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:backButton];
    
    
}



-(void)PlayerDetailsbackButtonAction:(id)sender{
    
//    [self removeFromSuperview];
    [self.delegatePlayerDetail PlayerDetailsbackButtonAction];
    
}


-(void)PlayerDetailsstartGameButtonAction{
    
//    if (!(firstPlayerTextfield.text.length <=0)  && !(secondPlayerTextfield.text.length <= 0))
//    {
//            UIAlertView* alertView1= [[UIAlertView alloc]initWithTitle:@"Name Empty" message:@"Player's Name cannot be empty" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
//            [alertView1 show];
//
//        }
//
//
    [self endEditing:TRUE];
    
    if ( [firstPlayerTextfield.text isEqualToString:@""] || [secondPlayerTextfield.text isEqualToString:@"" ]) {
        
        UIAlertView* alertView1= [[UIAlertView alloc]initWithTitle:@"Name Empty" message:@"Player's Name cannot be empty" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        [alertView1 show];
    }
    else if ( ![firstPlayerTextfield.text isEqualToString:@""] || ![secondPlayerTextfield.text isEqualToString:@"" ]){
//        mainGameViewObject= [[MainGameView alloc]initWithFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)];
//        [mainGameViewObject mainGameViewElements];
//        mainGameViewObject.backgroundColor=[UIColor greenColor];
//        [self addSubview:mainGameViewObject];
        
        [self.delegatePlayerDetail PlayerDetailsstartGameButtonAction];
        
    }
    

    
 
}


-(BOOL) textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}


-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    [self endEditing:true];
    [super touchesBegan:touches withEvent:event];
}


@end
