//
//  ViewController.m
//  TicTacToe
//
//  Created by click labs136 on 10/13/15.
//  Copyright (c) 2015 click labs136. All rights reserved.
//

#import "ViewController.h"
#import "StartupScreenView.h"
#import "PlayerDetailsView.h"
#import "SettingsView.h"
#import "MainGameView.h"

@interface ViewController ()<StartupScreenViewDelgatePr,SettingsViewDelegateProtocol,MainGameViewDelegateProtocol,PlayerDetailsViewDelegateProtocol>

{
    StartupScreenView *startupScreenViewObject;
    PlayerDetailsView *playerDetailsViewObject;
    SettingsView *settingsViewObject;
    MainGameView* mainGameViewObject;
    
}

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    startupScreenViewObject = [[StartupScreenView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    startupScreenViewObject.backgroundColor = [UIColor blueColor];
    [startupScreenViewObject startupScreenElements];
    [self.view addSubview:startupScreenViewObject];
  
//    [self soundAction];
    startupScreenViewObject.delegate=self;
}






- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}



#pragma -StartupScreenViewActions;


-(void)startButtonAction{

        playerDetailsViewObject=[[PlayerDetailsView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
        playerDetailsViewObject.backgroundColor=[UIColor blueColor];
        [playerDetailsViewObject playerDetailsViewElements];
        playerDetailsViewObject.delegatePlayerDetail=self;
        [self.view addSubview:playerDetailsViewObject];

}
-(void)settingButtonAction{

        settingsViewObject= [[SettingsView alloc]initWithFrame:CGRectMake(00, 00, self.view.frame.size.width, self.view.frame.size.height)];
        settingsViewObject.backgroundColor=[UIColor grayColor];
        [settingsViewObject settingsViewElements];
        settingsViewObject.delgateback=self;
        [self.view addSubview:settingsViewObject];
}



#pragma - SettingsviewActions:

-(void)backButtonAction{

    startupScreenViewObject = [[StartupScreenView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    startupScreenViewObject.backgroundColor = [UIColor blueColor];
    [startupScreenViewObject startupScreenElements];
    startupScreenViewObject.delegate=self;
    //playerDetailsViewObject.delegatePlayerDetail=self;
    [self.view addSubview:startupScreenViewObject];
}




#pragma -MainGameViewActions : 

-(void)mainGameBack{
    
    
    startupScreenViewObject = [[StartupScreenView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    startupScreenViewObject.backgroundColor = [UIColor blueColor];
    [startupScreenViewObject startupScreenElements];
    startupScreenViewObject.delegate=self;
    //playerDetailsViewObject.delegatePlayerDetail=self;
    //mainGameViewObject.mainDelegate=self;
    [self.view addSubview:startupScreenViewObject];
    
//    startupScreenViewObject = [[StartupScreenView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
//    startupScreenViewObject.backgroundColor = [UIColor blueColor];
//    [startupScreenViewObject startupScreenElements];
//    startupScreenViewObject.delegate=self;
//    playerDetailsViewObject.delegatePlayerDetail=self;
//    mainGameViewObject.mainDelegate=self;
//    [self.view addSubview:startupScreenViewObject];
}




#pragma PlayerDetailsViewActions :

-(void)PlayerDetailsbackButtonAction{
    
    startupScreenViewObject = [[StartupScreenView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    startupScreenViewObject.backgroundColor = [UIColor blueColor];
    [startupScreenViewObject startupScreenElements];
    startupScreenViewObject.delegate=self;
    //playerDetailsViewObject.delegatePlayerDetail=self;
   // mainGameViewObject.mainDelegate=self;
    [self.view addSubview:startupScreenViewObject];
}
-(void)PlayerDetailsstartGameButtonAction{

    //startupScreenViewObject.delegate=self;
   // playerDetailsViewObject.delegatePlayerDetail=self;
    
    mainGameViewObject= [[MainGameView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    [mainGameViewObject mainGameViewElements];
    mainGameViewObject.mainDelegate=self;
    [self.view addSubview:mainGameViewObject];
}



@end
